//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.02.13 at 02:10:28 PM EET 
//


package com.nixsolutions.silin.pmx.domain;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.nixsolutions.silin.pmx.domain package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.nixsolutions.silin.pmx.domain
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Content }
     * 
     */
    public Content createContent() {
        return new Content();
    }

    /**
     * Create an instance of {@link SongType }
     * 
     */
    public SongType createSongType() {
        return new SongType();
    }

    /**
     * Create an instance of {@link PodcastType }
     * 
     */
    public PodcastType createPodcastType() {
        return new PodcastType();
    }

    /**
     * Create an instance of {@link AudiobookType }
     * 
     */
    public AudiobookType createAudiobookType() {
        return new AudiobookType();
    }

}
