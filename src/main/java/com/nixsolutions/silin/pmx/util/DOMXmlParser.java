package com.nixsolutions.silin.pmx.util;

import com.nixsolutions.silin.pmx.domain.*;
import com.nixsolutions.silin.pmx.util.helpers.PmxHelpers;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Created by silin on 13.02.17.
 */
@Service
public class DOMXmlParser {

    public Content readXML(String xml) {
        Content content = new Content();
        try (InputStream inputStream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))) {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(inputStream);
            document.getDocumentElement().normalize();
            NodeList nodes = document.getDocumentElement().getChildNodes();
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    AudioFileType audioFile = null;
                    if (element.getTagName().equals(PmxConstants.ELEMENT_SONG)) {
                        audioFile = readSongElement(element);
                    }
                    else if (element.getTagName().equals(PmxConstants.ELEMENT_AUDIOBOOK)) {
                        audioFile = readAudiobookElement(element);
                    }
                    else {
                        audioFile = readPodcastElement(element);
                    }
                    content.getSongOrPodcastOrAudiobook().add(audioFile);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return content;
    }

    public String writeXML(Content content) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element contentElement = document.createElement(PmxConstants.ELEMENT_CONTENT);
            document.appendChild(contentElement);
            List<AudioFileType> files = content.getSongOrPodcastOrAudiobook();
            for (AudioFileType file: files) {
                if (file instanceof SongType) {
                    contentElement.appendChild(writeSongElement((SongType) file, document));
                }
                else if (file instanceof AudiobookType) {
                    contentElement.appendChild(writeAudiobookElement((AudiobookType) file, document));
                }
                else if (file instanceof PodcastType) {
                    contentElement.appendChild(writePodcastElement((PodcastType) file, document));
                }
            }

            return PmxHelpers.convertXmlDocumentToString(document);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    private SongType readSongElement(Element element) {
        SongType song = new SongType();
        song = (SongType) readCommonElements(song, element);
        NodeList nodes = element.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element songProperty = (Element) node;
                String tagName = songProperty.getTagName();
                if (tagName.equals(PmxConstants.ELEMENT_PERFORMER)) {
                    song.setPerformer(songProperty.getTextContent());
                }
                else if (tagName.equals(PmxConstants.ELEMENT_ALBUM)) {
                    song.setAlbum(songProperty.getTextContent());
                }
                else if (tagName.equals(PmxConstants.ELEMENT_YEAR)) {
                    song.setYear(songProperty.getTextContent());
                }
            }
        }
        return song;
    }

    private Element writeSongElement(SongType song, Document document) {
        Element songElement = document.createElement(PmxConstants.ELEMENT_SONG);
        songElement = writeCommonElements(song, songElement, document);
        String performer = song.getPerformer();
        String album = song.getAlbum();
        String year = song.getYear();

        if ((performer != null) && StringUtils.isNotBlank(performer)) {
            Element performerElement = document.createElement(PmxConstants.ELEMENT_PERFORMER);
            performerElement.setTextContent(performer);
            songElement.appendChild(performerElement);
        }
        if ((album != null) && StringUtils.isNotBlank(album)) {
            Element albumElement = document.createElement(PmxConstants.ELEMENT_ALBUM);
            albumElement.setTextContent(album);
            songElement.appendChild(albumElement);
        }
        if ((year != null) && StringUtils.isNotBlank(year)) {
            Element yearElement = document.createElement(PmxConstants.ELEMENT_YEAR);
            yearElement.setTextContent(year);
            songElement.appendChild(yearElement);
        }
        return songElement;
    }

    private AudiobookType readAudiobookElement(Element element) {
        AudiobookType audiobook = new AudiobookType();
        audiobook = (AudiobookType) readCommonElements(audiobook, element);
        NodeList nodes = element.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element audioBookProperty = (Element) node;
                String tagName = audioBookProperty.getTagName();
                if (tagName.equals(PmxConstants.ELEMENT_AUTHOR)) {
                    audiobook.setAuthor(audioBookProperty.getTextContent());
                }
                else if (tagName.equals(PmxConstants.ELEMENT_GENRE)) {
                    audiobook.setGenre(audioBookProperty.getTextContent());
                }
                else if (tagName.equals(PmxConstants.ELEMENT_LANGUAGE)) {
                    audiobook.setLanguage(audioBookProperty.getTextContent());
                }
                else if (tagName.equals(PmxConstants.ELEMENT_YEAR)) {
                    audiobook.setYear(audioBookProperty.getTextContent());
                }
            }
        }
        return audiobook;
    }

    private Element writeAudiobookElement(AudiobookType audiobook, Document document) {
        Element audiobookElement = document.createElement(PmxConstants.ELEMENT_AUDIOBOOK);
        audiobookElement = writeCommonElements(audiobook, audiobookElement, document);
        String author = audiobook.getAuthor();
        String genre = audiobook.getGenre();
        String language = audiobook.getLanguage();
        String year = audiobook.getYear();

        if ((author != null) && StringUtils.isNotBlank(author)) {
            Element authorElement = document.createElement(PmxConstants.ELEMENT_AUTHOR);
            authorElement.setTextContent(author);
            audiobookElement.appendChild(audiobookElement);
        }
        if ((genre != null) && StringUtils.isNotBlank(genre)) {
            Element genreElement = document.createElement(PmxConstants.ELEMENT_GENRE);
            genreElement.setTextContent(genre);
            audiobookElement.appendChild(genreElement);
        }
        if ((language != null) && StringUtils.isNotBlank(language)) {
            Element languageElement = document.createElement(PmxConstants.ELEMENT_LANGUAGE);
            languageElement.setTextContent(language);
            audiobookElement.appendChild(languageElement);
        }
        if ((year != null) && StringUtils.isNotBlank(year)) {
            Element yearElement = document.createElement(PmxConstants.ELEMENT_YEAR);
            yearElement.setTextContent(year);
            audiobookElement.appendChild(yearElement);
        }
        return audiobookElement;
    }

    private PodcastType readPodcastElement(Element element) throws DatatypeConfigurationException {
        PodcastType podcast = new PodcastType();
        podcast = (PodcastType) readCommonElements(podcast, element);
        NodeList nodes = element.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element podcastProperty = (Element) node;
                String tagName = podcastProperty.getTagName();
                if (tagName.equals(PmxConstants.ELEMENT_TOPIC)) {
                    podcast.setTopic(podcastProperty.getTextContent());
                }
                else if (tagName.equals(PmxConstants.ELEMENT_EPISODE)) {
                    podcast.setEpisode(BigInteger
                            .valueOf(Long.valueOf(podcastProperty.getTextContent())));
                }
                else if (tagName.equals(PmxConstants.ELEMENT_DATE)) {
                    podcast.setDate(DatatypeFactory
                            .newInstance()
                            .newXMLGregorianCalendar(podcastProperty.getTextContent()));
                }
            }

        }
        return podcast;
    }

    private Element writePodcastElement(PodcastType podcast, Document document) {
        Element podcastElement = document.createElement(PmxConstants.ELEMENT_PODCAST);
        podcastElement = writeCommonElements(podcast, podcastElement, document);
        String topic = podcast.getTopic();
        BigInteger episode = podcast.getEpisode();
        XMLGregorianCalendar date = podcast.getDate();

        if ((topic != null) && StringUtils.isNotBlank(topic)) {
            Element topicElement = document.createElement(PmxConstants.ELEMENT_TOPIC);
            topicElement.setTextContent(topic);
            podcastElement.appendChild(topicElement);
        }
        if (episode != null) {
            Element episodeElement = document.createElement(PmxConstants.ELEMENT_EPISODE);
            episodeElement.setTextContent(String.valueOf(episode));
            podcastElement.appendChild(episodeElement);
        }
        if (date != null) {
            Element dateElement = document.createElement(PmxConstants.ELEMENT_DATE);
            dateElement.setTextContent(date.toXMLFormat());
            podcastElement.appendChild(dateElement);
        }
        return podcastElement;
    }

    private AudioFileType readCommonElements(AudioFileType audioFile, Element element) {
        NodeList nodes = element.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element audioFileProperty = (Element) node;
                String tagName = audioFileProperty.getTagName();
                if (tagName.equals(PmxConstants.ELEMENT_FILE_ID)) {
                    audioFile.setFileId(Long
                            .valueOf(audioFileProperty.getTextContent()));
                }
                else if (tagName.equals(PmxConstants.ELEMENT_FILE_NAME)) {
                    audioFile.setFileName(audioFileProperty.getTextContent());
                }
                else if (tagName.equals(PmxConstants.ELEMENT_FILE_CONTENT_NAME)) {
                    audioFile.setFileContentName(audioFileProperty.getTextContent());
                }
                else if (tagName.equals(PmxConstants.ELEMENT_FILE_DURATION)) {
                    audioFile.setFileDuration(BigInteger
                            .valueOf(Long.valueOf(audioFileProperty.getTextContent())));
                }
                else if (tagName.equals(PmxConstants.ELEMENT_FILE_SIZE)) {
                    audioFile.setFileSize(BigDecimal
                            .valueOf(Double.valueOf(audioFileProperty.getTextContent())));
                }
                else if (tagName.equals(PmxConstants.ELEMENT_PRICE)) {
                    audioFile.setPrice(BigDecimal
                            .valueOf(Double.valueOf(audioFileProperty.getTextContent())));
                }
                else if (tagName.equals(PmxConstants.ELEMENT_DESCRIPTION)) {
                    audioFile.setDescription(audioFileProperty.getTextContent());
                }
            }
        }
        return audioFile;
    }

    private Element writeCommonElements(AudioFileType audioFile, Element element, Document document) {
        long fileId = audioFile.getFileId();
        String fileName = audioFile.getFileName();
        String fileContentName = audioFile.getFileContentName();
        BigInteger fileDuration = audioFile.getFileDuration();
        BigDecimal fileSize = audioFile.getFileSize();
        BigDecimal price = audioFile.getPrice();
        String description = audioFile.getDescription();

        if (fileId != 0) {
            Element fileIdElement = document.createElement(PmxConstants.ELEMENT_FILE_ID);
            fileIdElement.setTextContent(String.valueOf(fileId));
            element.appendChild(fileIdElement);
        }
        if ((fileName != null) && StringUtils.isNotBlank(fileName)) {
            Element fileNameElement = document.createElement(PmxConstants.ELEMENT_FILE_NAME);
            fileNameElement.setTextContent(fileName);
            element.appendChild(fileNameElement);
        }
        if ((fileContentName != null) && StringUtils.isNotBlank(fileContentName)) {
            Element fileContentNameElement = document.createElement(PmxConstants.ELEMENT_FILE_CONTENT_NAME);
            fileContentNameElement.setTextContent(fileContentName);
            element.appendChild(fileContentNameElement);
        }
        if (fileDuration != null) {
            Element fileDurationElement = document.createElement(PmxConstants.ELEMENT_FILE_DURATION);
            fileDurationElement.setTextContent(fileDuration.toString());
            element.appendChild(fileDurationElement);
        }
        if (fileSize != null) {
            Element fileSizeElement = document.createElement(PmxConstants.ELEMENT_FILE_SIZE);
            fileSizeElement.setTextContent(fileSize.toString());
            element.appendChild(fileSizeElement);
        }
        if (price != null) {
            Element priceElement = document.createElement(PmxConstants.ELEMENT_PRICE);
            priceElement.setTextContent(price.toString());
            element.appendChild(priceElement);
        }
        if ((description != null) && StringUtils.isNotBlank(description)) {
            Element descriptionElement = document.createElement(PmxConstants.ELEMENT_DESCRIPTION);
            descriptionElement.setTextContent(description);
            element.appendChild(descriptionElement);
        }
        return element;
    }
}
