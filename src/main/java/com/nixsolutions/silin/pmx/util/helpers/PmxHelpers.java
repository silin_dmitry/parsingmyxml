package com.nixsolutions.silin.pmx.util.helpers;

import com.nixsolutions.silin.pmx.domain.Content;
import com.nixsolutions.silin.pmx.domain.PodcastType;
import com.nixsolutions.silin.pmx.domain.SongType;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.w3c.dom.Document;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.GregorianCalendar;

/**
 * Created by diman on 13.02.17.
 */
public class PmxHelpers {

    public static Content prepareContent() {
        SongType song1 = new SongType();
        song1.setFileId(1);
        song1.setFileName("Muse_Hysteria.mp3");
        song1.setFileContentName("Muse - Hysteria");
        song1.setFileDuration(BigInteger.valueOf(227));
        song1.setFileSize(BigDecimal.valueOf(7.43));
        song1.setPerformer("Muse");
        song1.setAlbum("Absolution");
        song1.setYear("2003");

        SongType song2 = new SongType();
        song2.setFileId(324);
        song2.setFileName("39nth.mp3");
        song2.setFileContentName("Queen - '39");
        song2.setFileDuration(BigInteger.valueOf(210));
        song2.setFileSize(BigDecimal.valueOf(6.01));
        song2.setPrice(BigDecimal.valueOf(0.99));
        song2.setDescription("some description here");
        song2.setPerformer("Queen");
        song2.setAlbum("A Night At The Opera");
        song2.setYear("1975");

        PodcastType podcast1 = new PodcastType();
        podcast1.setFileId(34436);
        podcast1.setFileName("startalk_radio_201.mp3");
        podcast1.setFileContentName("StarTalk Radio - Episode 201 - Comedy and Cars With Jay Leno");
        podcast1.setFileDuration(BigInteger.valueOf(9360));
        podcast1.setFileSize(BigDecimal.valueOf(56.77));
        try {
            podcast1.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar("2017-02-11"));
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        podcast1.setEpisode(BigInteger.valueOf(126));
        podcast1.setTopic("Comedy and Cars with Jay Leno");

        Content content = new Content();
        content.getSongOrPodcastOrAudiobook().add(song1);
        content.getSongOrPodcastOrAudiobook().add(song2);
        content.getSongOrPodcastOrAudiobook().add(podcast1);

        return content;
    }

    public static String convertXmlDocumentToString(Document document) {
        String xmlString = null;
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try (StringWriter stringWriter = new StringWriter()) {
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(stringWriter);
            transformer.transform(domSource, streamResult);
            xmlString = stringWriter.getBuffer().toString();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return xmlString;
    }

}
