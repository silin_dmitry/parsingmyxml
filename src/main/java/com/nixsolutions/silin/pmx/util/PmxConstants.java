package com.nixsolutions.silin.pmx.util;

/**
 * Created by silin on 16.02.17.
 */
public class PmxConstants {

    public static final String ELEMENT_CONTENT = "content";

    public static final String ELEMENT_SONG = "song";

    public static final String ELEMENT_AUDIOBOOK = "audiobook";

    public static final String ELEMENT_PODCAST = "podcast";

    public static final String ELEMENT_FILE_ID = "fileId";

    public static final String ELEMENT_FILE_NAME = "fileName";

    public static final String ELEMENT_FILE_CONTENT_NAME = "fileContentName";

    public static final String ELEMENT_FILE_DURATION = "fileDuration";

    public static final String ELEMENT_FILE_SIZE = "fileSize";

    public static final String ELEMENT_PRICE = "price";

    public static final String ELEMENT_DESCRIPTION = "description";

    public static final String ELEMENT_YEAR = "year"; //used for both SONG and AUDIOBOOK

    /*SONG SPECIFIC ELEMENTS*/
    public static final String ELEMENT_PERFORMER = "performer";

    public static final String ELEMENT_ALBUM = "album";

    /*AUDIOBOOK SPECIFIC ELEMENTS*/
    public static final String ELEMENT_AUTHOR = "author";

    public static final String ELEMENT_GENRE = "genre";

    public static final String ELEMENT_LANGUAGE = "language";

    /*PODCAST SPECIFIC ELEMENTS*/
    public static final String ELEMENT_TOPIC = "topic";

    public static final String ELEMENT_EPISODE = "episode";

    public static final String ELEMENT_DATE = "date";


}
