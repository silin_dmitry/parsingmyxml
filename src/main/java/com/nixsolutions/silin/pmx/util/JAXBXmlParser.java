package com.nixsolutions.silin.pmx.util;

import com.nixsolutions.silin.pmx.domain.Content;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created by silin on 13.02.17.
 */
@Service
public class JAXBXmlParser {

    public Content readXML(String xml) {
        JAXBContext context = null;
        Content content = null;
        try (StringReader reader = new StringReader(xml)) {
            context = JAXBContext.newInstance(Content.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            content = (Content) unmarshaller.unmarshal(reader);
        } catch (JAXBException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return content;
    }

    public String writeXML(Content content) {
        String xml = null;
        try (StringWriter writer = new StringWriter()) {
            JAXBContext context = JAXBContext.newInstance(Content.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(content, writer);
            xml = writer.toString();
        } catch (JAXBException e) {
            e.printStackTrace();
            throw new RuntimeException();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return xml;
    }

}
