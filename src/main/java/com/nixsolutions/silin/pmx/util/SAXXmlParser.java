package com.nixsolutions.silin.pmx.util;

import com.nixsolutions.silin.pmx.domain.AudioFileType;
import com.nixsolutions.silin.pmx.domain.AudiobookType;
import com.nixsolutions.silin.pmx.domain.Content;
import com.nixsolutions.silin.pmx.domain.PodcastType;
import com.nixsolutions.silin.pmx.domain.SongType;
import org.springframework.stereotype.Service;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;



/**
 * Created by silin on 13.02.17.
 */
@Service
public class SAXXmlParser {

    public Content readXml(String xml) {
        try (InputStream inputStream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))) {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            PmxSAXHandler handler = new PmxSAXHandler();
            parser.parse(inputStream, handler);
            Content content = handler.getContent();
            return content;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Content();
    }

}

class PmxSAXHandler extends DefaultHandler {


    private Content content;

    private AudioFileType audioFile;

    private String currentlyProcessingType;

    private String textContent;

    @Override
    public void startDocument() throws SAXException {
        content = new Content();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        textContent = null;
        if (qName.equals(PmxConstants.ELEMENT_SONG)) {
            audioFile = new SongType();
            currentlyProcessingType = PmxConstants.ELEMENT_SONG;
        }
        if (qName.equals(PmxConstants.ELEMENT_AUDIOBOOK)) {
            audioFile = new AudiobookType();
            currentlyProcessingType = PmxConstants.ELEMENT_AUDIOBOOK;
        }
        if (qName.equals(PmxConstants.ELEMENT_PODCAST)) {
            audioFile = new PodcastType();
            currentlyProcessingType = PmxConstants.ELEMENT_PODCAST;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        textContent = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals(PmxConstants.ELEMENT_SONG)
                || qName.equals(PmxConstants.ELEMENT_AUDIOBOOK)
                || qName.equals(PmxConstants.ELEMENT_PODCAST)) {

            content.getSongOrPodcastOrAudiobook().add(audioFile);
            currentlyProcessingType = "";
        }
        else if (qName.equals(PmxConstants.ELEMENT_FILE_ID)) {
            audioFile.setFileId(Long.valueOf(textContent));
        }
        else if (qName.equals(PmxConstants.ELEMENT_FILE_NAME)) {
            audioFile.setFileName(textContent);
        }
        else if (qName.equals(PmxConstants.ELEMENT_FILE_CONTENT_NAME)) {
            audioFile.setFileContentName(textContent);
        }
        else if (qName.equals(PmxConstants.ELEMENT_FILE_DURATION)) {
            audioFile.setFileDuration(BigInteger
                    .valueOf(Long.valueOf(textContent)));
        }
        else if (qName.equals(PmxConstants.ELEMENT_FILE_SIZE)) {
            audioFile.setFileSize(BigDecimal
                    .valueOf(Double.valueOf(textContent)));
        }
        else if (qName.equals(PmxConstants.ELEMENT_PRICE)) {
            audioFile.setPrice(BigDecimal
                    .valueOf(Double.valueOf(textContent)));
        }
        else if (qName.equals(PmxConstants.ELEMENT_DESCRIPTION)) {
            audioFile.setDescription(textContent);
        }

        //Here go Song specific elements
        if (currentlyProcessingType.equals(PmxConstants.ELEMENT_SONG)) {
            if (qName.equals(PmxConstants.ELEMENT_PERFORMER)) {
                ((SongType) audioFile).setPerformer(textContent);
            }
            else if (qName.equals(PmxConstants.ELEMENT_ALBUM)) {
                ((SongType) audioFile).setAlbum(textContent);
            }
            else if (qName.equals(PmxConstants.ELEMENT_YEAR)) {
                ((SongType) audioFile).setYear(textContent);
            }
        }

        //Audiobook specific elements
        if (currentlyProcessingType.equals(PmxConstants.ELEMENT_AUDIOBOOK)) {
            if (qName.equals(PmxConstants.ELEMENT_AUTHOR)) {
                ((AudiobookType) audioFile).setAuthor(textContent);
            }
            else if (qName.equals(PmxConstants.ELEMENT_GENRE)) {
                ((AudiobookType) audioFile).setGenre(textContent);
            }
            else if (qName.equals(PmxConstants.ELEMENT_LANGUAGE)) {
                ((AudiobookType) audioFile).setLanguage(textContent);
            }
            else if (qName.equals(PmxConstants.ELEMENT_YEAR)) {
                ((AudiobookType) audioFile).setYear(textContent);
            }
        }

        //Podcast specific elements
        if (currentlyProcessingType.equals(PmxConstants.ELEMENT_PODCAST)) {
            if (qName.equals(PmxConstants.ELEMENT_TOPIC)) {
                ((PodcastType) audioFile).setTopic(textContent);
            }
            else if (qName.equals(PmxConstants.ELEMENT_EPISODE)) {
                ((PodcastType) audioFile).setEpisode(BigInteger
                        .valueOf(Long.valueOf(textContent)));
            }
            else if (qName.equals(PmxConstants.ELEMENT_DATE)) {
                try {
                    ((PodcastType) audioFile).setDate(DatatypeFactory
                            .newInstance().newXMLGregorianCalendar(textContent));
                } catch (DatatypeConfigurationException e) {
                    e.printStackTrace();
                    throw new RuntimeException();
                }
            }
        }

    }

    public Content getContent() {
        return content;
    }
}
