package com.nixsolutions.silin.pmx.web;

import com.nixsolutions.silin.pmx.domain.Content;
import com.nixsolutions.silin.pmx.util.DOMXmlParser;
import com.nixsolutions.silin.pmx.util.JAXBXmlParser;
import com.nixsolutions.silin.pmx.util.SAXXmlParser;
import com.nixsolutions.silin.pmx.util.helpers.PmxHelpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by silin on 13.02.17.
 */
@Controller
@RequestMapping("/pmx")
public class ApplicationController {

    @Autowired
    private JAXBXmlParser jaxbXmlParser;
    @Autowired
    private DOMXmlParser domXmlParser;
    @Autowired
    private SAXXmlParser saxXmlParser;

    @RequestMapping(value = "/jaxb", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void consumeXmlWithJAXB(@RequestBody String xml) {
        Content content = jaxbXmlParser.readXML(xml);
    }

    @RequestMapping(value = "/jaxb", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public @ResponseBody String produceXmlWithJAXB() {
        Content content = PmxHelpers.prepareContent();
        return jaxbXmlParser.writeXML(content);
    }

    @RequestMapping(value = "/dom", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void consumeXmlWithDOM(@RequestBody String xml) {
        Content content = domXmlParser.readXML(xml);
    }

    @RequestMapping(value = "/dom", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public @ResponseBody String produceXmlWithDOM() {
        Content content = PmxHelpers.prepareContent();
        return domXmlParser.writeXML(content);
    }

    @RequestMapping(value = "/sax", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void consumeXmlWithSAX(@RequestBody String xml) {
        Content content = saxXmlParser.readXml(xml);
    }
}
